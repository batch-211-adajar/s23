

let trainer = {
	
	Name: "Ash",
	Age: 10,
	Pokemon: ["Pikachu", "Charmander", "Bulbasaur"],
	Friends: { Name: 'Ron', address: 'Ph'  }, 
	Talk: function(){

		console.log("Pikachu! I choose you!");
	}



}

console.log("Results from dot notation:");
console.log(trainer.Name);
console.log(trainer.Age);
console.log(trainer.Pokemon);
console.log(trainer.Friends);


console.log("Results from bracket notation:");

console.log(trainer["Name"]);
console.log(trainer["Age"]);
console.log(trainer["Pokemon"]);
console.log(trainer["Friends"]);



trainer.Talk()


function pokemon(name,level,){
				this.name = name;
				this.level = level;
				this.health = level * 10
				this.attack = level * 2

				this.tackle = function(target){
				console.log(this.name + " tackled " + target.name);

			
				target.health -= this.attack

				console.log(target.name + " health is now reduced to " + target.health);

				if(target.health<=0){
					target.faint()
				}

			}
			this.faint = function(){
				console.log(this.name + " fainted.")
			}





			}


let pokemon1 = new pokemon('raticate', 5);
let pokemon2 = new pokemon('charmander', 8);
let pokemon3 = new pokemon('squirtle', 13);
let pokemon4 = new pokemon('Bulbasaur', 7);


